<?php

Route::namespace('Guest')->group(function () {

    Route::prefix('guest')->group(function () {
        Route::get('/register', 'RegisterController@register')->name('guest.register');
    });

});
